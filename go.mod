module gitlab.com/nickjwhite/mockvmsapi

go 1.20

require (
	gitlab.com/HnBI/seller-experience/libs/service v0.1.1
	gitlab.com/HnBI/seller-experience/vms/pkg/api/grpc/pbv1 v0.0.0-20230417085138-023dd2bc8c02
	google.golang.org/grpc v1.54.0
)

require (
	github.com/DataDog/datadog-agent/pkg/obfuscate v0.38.2 // indirect
	github.com/DataDog/datadog-go/v5 v5.1.1 // indirect
	github.com/DataDog/sketches-go v1.2.1 // indirect
	github.com/Microsoft/go-winio v0.5.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgraph-io/ristretto v0.1.0 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/go-qbit/qerror v1.2.3 // indirect
	github.com/go-qbit/rbac v0.0.0-20200326053441-69d621298d66 // indirect
	github.com/go-qbit/rpc v0.3.1 // indirect
	github.com/go-qbit/web v0.0.0-20190906164226-61a05e4df29c // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/glog v1.0.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.15.2 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/philhofer/fwd v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tinylib/msgp v1.1.2 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.23.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20230327215041-6ac7f18bb9d5 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.41.0 // indirect
)
