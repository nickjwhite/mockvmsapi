package config

import (
	"gitlab.com/HnBI/seller-experience/libs/service/config"
	"gitlab.com/HnBI/seller-experience/libs/service/grpc/interceptor/auth/jwt"
)

type Config struct {
	config.Base
	jwt.Config
}
