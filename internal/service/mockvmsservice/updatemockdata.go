//go:build ignore

// this can be run by `go generate`
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

const usage = `Usage: updatemockdata [-auth token] [-server addr]

Downloads the latest real data vms and saves it into the appropriate
location for mockvmsservice to use.`

func die(format string, a ...any) {
	fmt.Fprintf(os.Stderr, format+"\n", a...)
	os.Exit(1)
}

func main() {
	cases := []struct {
		req  string
		path string
	}{
		{req: "/grpc/get_vendors/v1", path: "getvendors.json"},
	}

	auth := flag.String("auth", "", "Authentication token to use with the service")
	server := flag.String("server", "vms-staging-internal-eks.eu-west-1.dev.hbi.systems", "Server to connect to")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *auth == "" {
		*auth = os.Getenv("AUTH")
	}
	if *auth == "" {
		fmt.Fprintln(os.Stderr, "Warning: empty authentication token will probably fail, set it with -auth or AUTH environment variable")
	}

	client := http.DefaultClient

	for _, c := range cases {
		fmt.Printf("Downloading %s to %s\n", c.req, c.path)
		req, err := http.NewRequest("POST", "https://"+*server+c.req, nil)
		if err != nil {
			die("Error making HTTP request: %v", err)
		}
		req.Header.Add("X-API-Key", *auth)
		resp, err := client.Do(req)
		if err != nil {
			die("Error doing HTTP request: %v", err)
		}
		if resp.StatusCode != 200 {
			die("HTTP request got status code %d", resp.StatusCode)
		}

		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			die("Failed to read HTTP body: %v", err)
		}

		err = ioutil.WriteFile(c.path, b, 0644)
		if err != nil {
			die("Failed to write file %s: %v", c.path, err)
		}
	}
}
