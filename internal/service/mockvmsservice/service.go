package mockvmsservice

//go:generate go run updatemockdata.go

import (
	"context"
	_ "embed"
	pb "gitlab.com/HnBI/seller-experience/vms/pkg/api/grpc/pbv1"
	"google.golang.org/protobuf/encoding/protojson"
)

//go:embed getvendors.json
var getVendorsJson []byte

type Service struct {
	pb.UnimplementedVmsServer
}

func New() *Service {
	return &Service{}
}

func (s *Service) GetVendorsV1(ctx context.Context, req *pb.GetVendorsRequestV1) (*pb.GetVendorsResponseV1, error) {
	// if there's getvendors.json is empty, just return a basic response
	if len(getVendorsJson) == 0 {
		return &pb.GetVendorsResponseV1{
			Vendors: []*pb.Vendor{
				{Id: 1, Name: "Vendor 1"},
				{Id: 2, Name: "Vendor 2"},
			},
		}, nil
	}

	vendors := pb.GetVendorsResponseV1{}
	err := protojson.Unmarshal(getVendorsJson, &vendors)
	if err != nil {
		return &vendors, err
	}
	return &vendors, nil
}
