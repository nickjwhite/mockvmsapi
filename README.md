# mockvmsapi

This is a simple service that mocks the VMS service. By default it
returns basic static results, but it can also return results which
exactly match what have been previously downloaded from the real
VMS service.

## setup

If you want to serve realistic results, get yourself an AUTH token
to access the real VMS service (ask a colleague for one), then run
`make`:
  export AUTH=abcd
  make

## run

`make run` will start the service on ports 8086 & 8087 for HTTP
and gRPC respectively. If you want to run it on a different port,
just set the NET_HTTP_ADDR and NET_GRPC_ADDR environment variables
to `:portnum`.

## customisation

If for any reason you want to alter the results that are returned,
play around with the json files that are downloaded by `make`; as
long as they're still valid to marshal into the appropriate
protobuf format you can change them however you like.
