all: generate

generate:
	go generate ./internal/service/mockvmsservice

run:
	NET_HTTP_ADDR=:8086 NET_GRPC_ADDR=:8087 go run ./cmd/mockvmsservice

.PHONY: run generate
