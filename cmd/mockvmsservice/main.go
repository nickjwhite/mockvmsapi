package main

import (
	"context"
	"gitlab.com/HnBI/seller-experience/libs/service"
	"gitlab.com/HnBI/seller-experience/libs/service/authctx"
	"gitlab.com/HnBI/seller-experience/libs/service/grpc/interceptor/auth/dbtoken"
	"gitlab.com/HnBI/seller-experience/libs/service/grpc/interceptor/auth/jwt"
	"gitlab.com/HnBI/seller-experience/libs/service/grpc/interceptor/auth/prevent"
	"gitlab.com/HnBI/seller-experience/libs/service/grpc/interceptor/logging"
	"gitlab.com/HnBI/seller-experience/libs/service/rbac"
	pb "gitlab.com/HnBI/seller-experience/vms/pkg/api/grpc/pbv1"
	"gitlab.com/nickjwhite/mockvmsapi/internal/config"
	"gitlab.com/nickjwhite/mockvmsapi/internal/service/mockvmsservice"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

const token = "TEST"

func main() {
	cfg := &config.Config{}

	s := service.New(cfg, []rbac.Role{}, service.Options{})

	s.InitGrpc(
		[]grpc.UnaryServerInterceptor{
			jwt.Interceptor(cfg, map[string]uint8{
				"admin": 1,
			}, func(ctx context.Context, name, email string) (*authctx.User, error) {
				return AuthorizeByJWTToken(ctx, name, email)
			}, s.GetLogger()),
			dbtoken.Interceptor(dbtoken.AuthorizerFunc(func(ctx context.Context, token string) (*authctx.User, error) {
				return AuthorizeByToken(ctx, token)
			}), s.GetLogger()),

			logging.Interceptor(s.GetLogger()),

			prevent.Interceptor(),
		},
		nil,
	)

	// Register gRPC
	grpcServer := mockvmsservice.New()
	pb.RegisterVmsServer(s.GetGrpcServer(), grpcServer)

	// Register gRPC Gateway
	if err := pb.RegisterVmsHandlerFromEndpoint(
		context.Background(), s.GetGrpcGw(nil), cfg.Net.GrpcAddr,
		[]grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())},
	); err != nil {
		panic(err)
	}

	s.Run()
}

func AuthorizeByJWTToken(_ context.Context, _ string, _ string) (*authctx.User, error) {
	return nil, nil
}

func AuthorizeByToken(_ context.Context, t string) (*authctx.User, error) {
	if t == token {
		return &authctx.User{
			Id:       1,
			Fullname: "admin",
			Email:    "admin@admin.com",
			Roles:    []uint8{},
		}, nil
	}

	return nil, status.Errorf(codes.Unauthenticated, "invalid token")

}
